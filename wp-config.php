<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nusa_eshop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$U`,mCQ5CDsIy9HZ3MKS$;C#!)| i[?[8z8d~pAFk=t 7wb%H_=H6:dnYni`jJe ');
define('SECURE_AUTH_KEY',  'iTwE|=W+BMNK=cJmBmrz?#3.`@$@}}9YX=Ze<:j3J<YNfz}2+`7;@<}+B~Zm$*HS');
define('LOGGED_IN_KEY',    '~r1OzHU/tH$TCJcqiBR]96:p##Z`l]4YWNCiax?1pw<:dD+B~cso@Q}Hb5EK$#g7');
define('NONCE_KEY',        'sz7`}<]4]+U@<j3hh!h5!xd;R]}h:t[)32>+D4)m{+hBea;bY8YnJobJ+Ns.GAQ-');
define('AUTH_SALT',        '`._ZG2s1qKyy,1DD|Gw!3urSbJtj9[+uthd3=ne]7G<VqVgVJ1U,S2]>PCJjzi0@');
define('SECURE_AUTH_SALT', ';$)84{-A|]uFW=/6$*1 ?:pW7!B2Y<828*9PKTp0B*<#~ $&EMp9;Oo3-NbVpE>9');
define('LOGGED_IN_SALT',   ',Q.zbbRH.Yg2wf#Zkv~]*kN9e9nb=Z+!Mtgl=bmD/k+/S%s$qf(KBn1ja@V>gUAa');
define('NONCE_SALT',       'vNtG(+^k iW*&aBU:($Xm[SC+H#/L%iG@[ugE7S_WBRn74%pbY7yuZE@s=zWSAw?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nusa_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
