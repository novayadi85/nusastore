<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>

<?php 
global $product , $post;
$wc = new WC_Product($post->ID);
$atts = $wc->get_attributes();
if(is_array($atts)  && sizeof($atts)){
	$attachment_ids = $product->get_gallery_attachment_ids();
	 $images = array();
	   $oattr = wc_get_product($product->id);
		$att = $oattr->get_variation_attributes();
		

		foreach ($att as $key => $attribute) {

			foreach ($attachment_ids as $attachment_id) {
				$svi_slug = get_post_meta($attachment_id, 'woosvi_slug', true);
				$woosvi_thumb = get_post_meta($attachment_id, 'woosvi_thumb', true);
				  //$images[strtolower($svi_slug)] = $attachment_id;
				  if($svi_slug && $woosvi_thumb){
					  $images[ucfirst($svi_slug)] = $attachment_id;
				  }
	//            if (strtolower($svi_slug) == strtolower($attribute))
	//                
			}
		}
	   
	$images = array_filter($images);
	$variants = $product->get_available_variations();

	foreach($variants as $b=> $var){
		if(isset($var["attributes"]["attribute_color"])){
			
			if(!empty($var["image_link"])){
				$attributes[$var["attributes"]["attribute_color"]] = $var["image_link"];
				
			}
			else{
				$attributes[$var["attributes"]["attribute_color"]]  = wp_get_attachment_image_src($images[$var["attributes"]["attribute_color"]],'thumbnail',true);
			}
			
		}
	}


	//Jika Data di ambil dari atribut variants
	if(sizeof($images)){
	   /*  echo "<ul style=\"margin: 0;padding: 0;\">";
		foreach($attributes as $key => $src){
			$title = $oattr->get_title() . " - " . $key;
			//list($src, $width, $height) = wp_get_attachment_image_src($img,'thumbnail',true);
			echo "<li class=\"another-choose\" data-title=\"{$title}\" data-variant=\"{$key}\" style=\"margin:5px;list-style: none;display: inline-block;\"><img src=\"{$src}\" width=\"50px\"></li>";
		} 
		echo "</ul>"; */
	}


	//jika data diambil dari variant thumb custom
	if(sizeof($images)){
		echo "<ul style=\"margin: 0;padding: 0;\">";
		foreach($images as $key => $img){
		  $title = $oattr->get_title() . " - " . $key;
		  list($src, $width, $height) = wp_get_attachment_image_src($img,'thumbnail',true);
			echo "<li class=\"another-choose\" data-title=\"{$title}\" data-variant=\"{$key}\" style=\"margin:5px;list-style: none;display: inline-block;\"><img src=\"{$src}\" width=\"50px\"></li>";
		} 
		
		echo "</ul>";
	}

?>
<div class="product-name-temp" style="display:none;"><?php echo $oattr->get_title();?></div>

<?php } ?>