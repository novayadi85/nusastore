if (!WOOSVI) {
    var WOOSVI = {};
} else {
    if (WOOSVI && typeof WOOSVI !== "object") {
        throw new Error("WOOSVI is not an Object type");
    }
}
WOOSVI.isLoaded = false;
WOOSVI.STARTS = function ($) {
    var $product_mainimg = $('a.woocommerce-main-image').html();
    var $form = $('.variations_form');
    var $container = $("div#woosvi_strap");
    var $product_variations = ($form.data('product_variations')) ? $form.data('product_variations') : '';
    var $lightGallery = [];
    var $methods;
    var $lightGallery_loaded = false;
    var $swselect = false;
    var $loadLens = false;
    return{NAME: "Application initialize module", VERSION: 3.8, init: function () {
            this.loadInits();
        },
        loadInits: function () {

            if ($form.find('.variations select').length > 0 || $('div.product').hasClass('product-type-simple')) {
                WOOSVI.STARTS.initLoadImages();
            }

            if ($form.find('.variations select').length > 0) {
                WOOSVI.STARTS.variationReset();
                WOOSVI.STARTS.variationChange();
            }

        },
        /*LOAD IMAGES ON PAGE*/

        initLoadImages: function () {

            if (WOOSVIDATA["hide_thumbs"] === '1')
                $container.find('div#woosvithumbs').hide();


            $lightGallery.push({
                'src': $($product_mainimg).attr('full'),
                'thumb': $($product_mainimg).attr('thumb')
            });

            var items = WOOSVI.STARTS.getItems();

            var cols = ' columns-' + WOOSVIDATA.columns;

            $container.fadeOut('fast', function () {

                $container.find('div#woosvimain').html('').append($product_mainimg);
                if ($container.find('ul.svithumbnails li').length > 0)
                    $container.find('ul.svithumbnails li').remove();
                else
                    $container.find('div#woosvithumbs').prepend('<ul class="svithumbnails' + cols + '"></ul>');


                WOOSVI.STARTS.loadImages(items);
                WOOSVI.STARTS.imagesLoader();
            }).removeClass('svihidden');
        },
        getItems: function () {
            var items = '';

            $.each($product_variations, function (i, v) {
                items += WOOSVI.STARTS.getImageItem(v.additional_images);
            });

            if (!items) {
                items = '';
                $.each(WOOSVIDATA.failsafe, function (i, v) {
                    items += WOOSVI.STARTS.getImageItem(v.additional_images);
                });
            }

            return items;
        },
        getImageItem: function (additional_images) {
            console.log(additional_images);
            var item = '';

            $.each(additional_images, function (i, ai) {
                item += '<li data-thumb="' + ai.thumb[0] + '" data-src="' + ai.full[0] + '">';
                item += '<div class="sviLoader_thumb"></div>';

                item += ai.img_thumb;
                if (WOOSVIDATA.slider === '1')
                    item += '</div>';
                else
                    item += '</li>';


                $lightGallery.push({
                    'src': ai.full[0],
                    'thumb': ai.thumb[0]
                });

            });

            return item;
        },
        loadImages: function (items) {
            var $slider_img;
            var $size = $(items).size() - 1;
            $.each($(items), function ($loop, v) {

                var $classes = [''];
                if ($loop === 0 || $loop % WOOSVIDATA.columns === 0) {
                    $classes.push('first');
                }
                if (($loop + 1) % WOOSVIDATA.columns === 0) {
                    $classes.push('last');
                }
                if ($loop === $size)
                    $classes.push('last');
                $(v).addClass($classes.join(' '));
                $container.find('ul.svithumbnails').append($(v));

            });
        },
        imagesLoader: function ($hide_thumbs) {
            $container.imagesLoaded().progress(WOOSVI.STARTS.onProgress).done(function (instance) {

                if (WOOSVIDATA.lightbox === '1') {
                    WOOSVI.STARTS.lightGalleryLoad();
                }

                $container.fadeIn('fast');
                if (WOOSVIDATA["hide_thumbs"] === '1' && $hide_thumbs) {
                    $container.find('div#woosvithumbs').slideDown();
                }

                WOOSVI.STARTS.ActivateSwapImage();

                WOOSVI.STARTS.LoadLens();


                if ($form.find('.variations select').length > 0) {
                    WOOSVI.STARTS.variationAjax();
                    WOOSVI.STARTS.variationChangeOnSelect();
                }

            });
        },
        onProgress: function (imgLoad, image) {
            var $item = $(image.img).parent();
            $item.find('.sviLoader_thumb').fadeOut().remove();
        },
        ActivateSwapImage: function () {
            $('ul.svithumbnails img').click(function (e) {
                WOOSVI.STARTS.initSwap(this);
            });
        },
        initSwap: function (v) {

            var image = new Image();
            $(v).parent().prepend('<div class="sviLoader_thumb"></div>');
            var svisingle = $(v).data('svisingle');

            image.src = $(svisingle).attr("src");

            $('div#woosvimain').prepend('<div class="sviLoader_thumb"></div>');

            $(image).on("load", function () {
                $('div#woosvimain img').fadeOut('fast').remove();
                $('div#woosvimain').prepend($(svisingle).hide());
                $('div#woosvimain img').fadeIn('fast');
                $('div#woosvimain').find('.sviLoader_thumb').fadeOut().remove();

                WOOSVI.STARTS.LoadLens();

                $('div.sviLoader_thumb').remove();

            });
        },
        /*END LOAD IMAGES ON PAGE*/
        /*LOAD lightGallery*/
        lightGalleryLoad: function () {
            var $lightWidth, $lightHeight, $lightmode, $lightAddClass;

            $methods = undefined;

            $methods = $('div#woosvimain');

            $methods.unbind('click').bind('click', function () {

                if ($lightGallery_loaded == true) {
                    $methods.data('lightGallery').destroy(true);
                }

                $lightWidth = 50 + '%';
                $lightHeight = 600 + 'px';
                $lightAddClass = 'fixed-size';
                $lightmode = 'lg-fade';

                $methods.lightGallery({
                    width: $lightWidth,
                    height: $lightHeight,
                    mode: $lightAddClass,
                    addClass: $lightmode,
                    dynamic: true,
                    dynamicEl: $lightGallery,
                    download: false
                });

                $methods.on('onAfterOpen.lg', function (event) {
                    var img = $('div#woosvimain').find('img').attr('src');

                    var ni;
                    var index = $('.svithumbnails li[data-src="' + img + '"]').index();


                    ni = index + 1;

                    if (index > 0) {

                        $methods.data('lightGallery').slide(ni);

                        $('div.lg-thumb-item.active').removeClass('active');
                        $('div.lg-thumb-item:eq(' + ni + ')').addClass('active');
                    }
                    $lightGallery_loaded = true;
                });

            });
        },
        /*END LOAD lightGallery*/
        /*LOAD LENS*/
        LoadLens: function () {
            if (WOOSVIDATA.lens !== '1')
                return;

            if ($loadLens)
                return;

            $loadLens = true;

            $("div.sviZoomContainer").remove();

            var ez, lensoptions;
            var ezR = setInterval(function () {
                if ($("div.sviZoomContainer").length <= 0) {
                    ez = $("div#woosvimain>img");
                    lensoptions = {
                        sviZoomType: 'lens',
                        lensShape: 'round',
                        lensSize: 150,
                        cursor: 'pointer',
                        galleryActiveClass: 'active',
                        containLensZoom: true,
                        loadingIcon: true,
                    };

                    ez.ezPlus(lensoptions);
                    $loadLens = false;
                    clearInterval(ezR);
                }
            }, 500);

        },
        /*END LOAD LENS*/
        /*VARIATIONS HANDLER*/
        variationAjax: function () {
            $(document).ajaxSend(function (event, jqxhr, settings) {
                if (settings.url.indexOf("wc-ajax=get_variation") >= 0) {
                    $container.hide();
                    $container.parent().append('<div class="sviLoader_thumb svihidden"></div>');
                    $container.parent().find('.sviLoader_thumb').hide().removeClass('svihidden').fadeIn();
                }
            }
            ).ajaxComplete(function (event, xhr, settings) {
                if (settings.url.indexOf("wc-ajax=get_variation") >= 0) {
                    $container.parent().find('.sviLoader_thumb').fadeOut().remove();
                    WOOSVI.STARTS.initLoadImages();
                }
            });
        },
        variationChange: function () {
            $form.on('show_variation', function (event, variation) {
                if (Object.keys(variation.additional_images).length > 0) //SE EXISTIREM VARIAÇÕES
                    WOOSVI.STARTS.variationSwap(variation);
            });
        },
        variationChangeOnSelect: function () {

            var all_attributes_chosen = true;

            if ($form.find('.variations select').size() <= 1)
                return;

            $form.find('.variations select').on('change', function () {

                $form.find('.variations select').each(function () {
                    if ($(this).val().length === 0) {
                        all_attributes_chosen = false;
                    }
                });

                if ($(this).val() !== '' && !all_attributes_chosen) {
                    $swselect = $(this).val();
                    if (WOOSVIDATA.img_groups[$swselect])
                        WOOSVI.STARTS.variationSwap(WOOSVIDATA.img_groups[$swselect], true);
                }

            });

        },
        variationReset: function () {
            $form.on('click', '.reset_variations', function (event) {
                $lightGallery = [];
                $('div#woosvimain').removeAttr('style');

                setTimeout(function () {
                    WOOSVI.STARTS.initLoadImages();
                }, 150)
            });
        },
        variationSwap: function ($variation, is_swselect) {
            $lightGallery = [];
            var items;
            if (is_swselect)
                items = WOOSVI.STARTS.getImageItem($variation);
            else
                items = WOOSVI.STARTS.getImageItem($variation.additional_images);

            $container.fadeOut('fast', function () {

                $container.find('div#woosvimain').html('').prepend($(items).first().find('img').data('svisingle'));
                $container.find('ul.svithumbnails li').remove();

                WOOSVI.STARTS.loadImages(items);
                WOOSVI.STARTS.imagesLoader(true);
            });
        }
    }
}(jQuery.noConflict());
jQuery(document).ready(function () {
    WOOSVI.STARTS.init();
});
