jQuery(document).ready(function($) {
	if(jQuery(".owl-carousel").length){
		jQuery(".owl-carousel").owlCarousel({
			singleItem : true,
			scrollPerPage:true,
			slideSpeed : 1000,
			navigation: true,
			pagination:true,
			lazyLoad : true,
			navigationText:["<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>","<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>"],
		});
	}
	
	
    jQuery('.another-choose img').click(function(){
       var color = jQuery(this).parent("li").attr("data-variant");
       jQuery('#color').val(color).change();
       jQuery('h1.product_title').html(jQuery(this).parent("li").attr("data-title"));
    });
    
    jQuery('#color').change(function(){
       var tmp_title = jQuery('.product-name-temp').html();
       jQuery('h1.product_title').html(jQuery(this).val() + "-" + tmp_title);
    });
});

function loadTitle(e){
	var tmp_title = jQuery('.product-name-temp').html();
	jQuery('h1.product_title').html(tmp_title + "-"+e.attribute_color);
}

function loadVariantImages($variation, is_swselect){
	jQuery.ajax({
		type: "POST",
		url: woocs_ajaxurl,
		dataType:'json',
		data: {
			variation: $variation,
			action:"get_image_variant"
			
		},
		beforeSend: function(){
			jQuery("#woosvimain").addClass("waiting");
			jQuery("#woosvimain").html("<img style=\"width: 30px; display: block;\" with=\"100px\" src=\"http://tromborg16.dev5.siteloom.dk/files/design/images/tromborg/AjaxLoader.gif\">");	 
		},
		success: function(data) {
			jQuery("#woosvimain").html(data.html);
		},	
		complete : function (){
			jQuery("#woosvimain").data('owlCarousel').reinit();
			jQuery("#woosvimain").removeClass("waiting");
		}
	});
}