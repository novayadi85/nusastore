<?php
/**
 * Theme functions.
 *
 * @author 		Nusa
 * @package 	Nusa
 * @version     1.0.0
 */

 
/**
 * Enqueue styles from the parent theme.
 */
function enqueue_styles() {

    $parent_style = 'freestore';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( "owl-carousel", get_template_directory_uri() . '/includes/owl-carousel/owl.carousel.css' );
    wp_enqueue_style( "owl-carousel-theme", get_template_directory_uri() . '/includes/owl-carousel/owl.theme.css' );
    
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    
    wp_enqueue_script( "custom_js", get_template_directory_uri() . '/js/script.js' );
    wp_enqueue_script( "owl_js", get_template_directory_uri() . '/includes/owl-carousel/owl.carousel.js' );
    
}


//Making jQuery Google API
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', false, '1.8.1');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'modify_jquery');


add_action( 'wp_enqueue_scripts', 'enqueue_styles' );
 
/**
 * Change currency symbols to currency codes.
 */

function change_currency_symbol( $symbols ) {
     $symbols['USD'] = 'USD ';
     $symbols['IDR'] = 'IDR ';

     return $symbols;
}
add_filter( 'woocommerce_currency_symbols', 'change_currency_symbol' );

if( isset( $_GET['showall'] ) ){ 
    add_filter( 'loop_shop_per_page', create_function( '$cols', 'return -1;' ) ); 
} else {
	if( isset($_GET['per_page']) )
	{
		add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '.$_GET['per_page'].';' ) );
	}
}

/** Step 2 (from text above). */
add_action( 'admin_menu', 'nusadebali_social_media_menu' );

/** Step 1. */
function nusadebali_social_media_menu() {
	//add_submenu_page( 'themes.php', 'Social Media Options', 'Social Media', 'manage_options', 'nusadebali-social-media', 'nusadebali_social_media_options' );
}

/*
* replace read more buttons for out of stock items
**/
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
	function woocommerce_template_loop_add_to_cart() {
		global $product;
		if (!$product->is_in_stock()) {
			echo '<a href="'.get_permalink().'" rel="nofollow" class="button out_of_stock_button">Out of Stock</a>';
		}
		else
		{
			wc_get_template('loop/add-to-cart.php');
		}
	}
}

/** Step 3. */
function nusadebali_social_media_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
?>
	<div class="wrap woocommerce">
		<h1>Social Media</h1>
		<nav class="nav-tab-wrapper woo-nav-tab-wrapper">
			<a href="&tab=facebook" class="nav-tab">Facebook</a>
			<a href="#" class="nav-tab">Twitter</a>
			<a href="#" class="nav-tab">Pinterest</a>
			<a href="#" class="nav-tab">Google+</a>
		</nav>
		<form name="form1" method="post" action="">
			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" class="titledesc">
							Share to Facebook?
						</th>
						<td class="forminp">
							<fieldset>
								<legend class="screen-reader-text"><span>Share to Facebook?</span></legend>
								<label for="enable_facebook">
									<input name="enable_facebook" id="enable_facebook" type="checkbox" class=""> Check this box to enable sharing to Facebook
								</label>
							</fieldset>
						</td>
					</tr>
				</tbody>
			</table>
			<hr>
			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" class="titledesc">
							
						</th>
						<td class="forminp">
							<fieldset>
								<legend class="screen-reader-text"><span>Share to Facebook?</span></legend>
								<label for="enable_facebook">
									<input name="enable_facebook" id="enable_facebook" type="checkbox" class=""> Check this box to enable sharing to Facebook
								</label>
							</fieldset>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	
<?php

}

add_action( 'wp_ajax_get_image_variant', 'prefix_ajax_get_image_variant' );
add_action( 'wp_ajax_nopriv_get_image_variant', 'prefix_ajax_get_image_variant' );

function prefix_ajax_get_image_variant() {
	if(sizeof($_REQUEST["variation"]) > 0 ){
		$html = array();
		foreach($_REQUEST["variation"]["additional_images"] as $image){
			$html[] = "<div class=\"item-images\">";
			$html[] = stripslashes($image["img"]);
			$html[] = "</div>";
		}
	}
	echo json_encode(array("html" => join($html)));
    wp_die();
}