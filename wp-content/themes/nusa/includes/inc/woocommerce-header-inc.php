<?php
// Ensure cart contents update when products are added to the cart via AJAX
add_filter('add_to_cart_fragments', 'nusa_wc_header_add_to_cart_fragment');
 
function nusa_wc_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    
    ob_start(); ?>
        <a class="header-cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'freestore'); ?>">
            <span class="header-cart-checkout <?php echo ( $woocommerce->cart->cart_contents_count > 0 ) ? sanitize_html_class( 'cart-has-items' ) : ''; ?>">
                <i class="fa fa-shopping-cart"></i>
            </span>
            <span class="header-cart-amount">
                <strong><?php _e('Cart', 'freestore'); ?></strong> <?php echo sprintf( _n( '%d', '%d', $woocommerce->cart->cart_contents_count, 'freestore' ), $woocommerce->cart->cart_contents_count ); ?> <?php $woocommerce->cart->cart_contents_count == 1 ? _e('Product', 'freestore') : _e('Products', 'freestore'); ?>
            </span>
        </a>
    <?php
    $fragments['a.header-cart-contents'] = ob_get_clean();
    
    return $fragments;
}
