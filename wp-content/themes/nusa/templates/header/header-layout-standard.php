<?php
/**
 * @package FreeStore
 */
global $woocommerce; ?>

<header id="masthead" class="site-header">
	
	<?php if ( ! get_theme_mod( 'freestore-header-remove-topbar' ) ) : ?>
	<div class="site-header-topbar">
		<div class="site-container">
			
			<div class="site-topbar-left">
				<?php wp_nav_menu( array( 'theme_location' => 'top-bar-menu', 'fallback_cb' => false ) ); ?>
				
				<?php if ( !get_theme_mod( 'freestore-header-remove-add' ) ) : ?>
                	<span class="site-topbar-left-ad"><i class="fa fa-map-marker"></i> <?php echo wp_kses_post( get_theme_mod( 'freestore-website-site-add', 'Cape Town, South Africa' ) ) ?></span>
                <?php endif; ?>
			</div>
			
			<div class="site-topbar-right">
				<?php if ( !get_theme_mod( 'freestore-header-remove-no' ) ) : ?>
                	<span class="site-topbar-right-no"><i class="fa fa-phone"></i> <?php echo wp_kses_post( get_theme_mod( 'freestore-website-head-no', 'Call Us: +2782 444 YEAH' ) ) ?></span>
				<?php endif; ?>
				
				<?php if ( !get_theme_mod( 'freestore-header-hide-social' ) ) : ?>
					<?php get_template_part( '/templates/social-links' ); ?>
				<?php endif; ?>
			</div>
			
			<div class="clearboth"></div>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="site-container">
		
		<?php if ( !get_theme_mod( 'freestore-header-search', false ) ) : ?>
		    <div class="search-block">
		        <?php get_search_form(); ?>
		    </div>
		<?php endif; ?>
		
		<div class="site-branding">
			<?php if ( has_custom_logo() ) : ?>
				<?php the_custom_logo(); ?>
		    <?php else : ?>
		        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		        <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		    <?php endif; ?>
		</div><!-- .site-branding -->
		
		<?php if ( !get_theme_mod( 'freestore-header-search', false ) ) : ?>
			<div class="menu-search">
		    	<i class="fa fa-search search-btn"></i>
		    </div>
		<?php endif; ?>
		
		<?php if ( freestore_is_woocommerce_activated() ) : ?>
			<?php if ( !get_theme_mod( 'freestore-header-remove-cart' ) ) : ?>
				<div class="header-cart">
				
					<a class="header-cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'freestore'); ?>">
						<span class="header-cart-checkout <?php echo ( $woocommerce->cart->cart_contents_count > 0 ) ? sanitize_html_class( 'cart-has-items' ) : ''; ?>">
							<i class="fa fa-shopping-cart"></i>
						</span>
						<span class="header-cart-amount">
							<strong><?php _e('Cart', 'freestore'); ?></strong> <?php echo sprintf( _n( '%d', '%d', $woocommerce->cart->cart_contents_count, 'freestore' ), $woocommerce->cart->cart_contents_count ); ?> <?php $woocommerce->cart->cart_contents_count == 1 ? _e('Product', 'freestore') : _e('Products', 'freestore'); ?>
						</span>
					</a>
					
					<ul class="header-cart-list">
						<?php if ($woocommerce->cart->cart_contents_count > 0): ?>
							<?php $items = $woocommerce->cart->get_cart(); foreach($items as $item => $values): ?>
								<li class="header-cart-item">
									<?php $_product = apply_filters( 'woocommerce_cart_item_product', $values['data'], $values, $item ); ?>
									<?php $product_id = apply_filters( 'woocommerce_cart_item_product_id', $values['product_id'], $values, $key ); ?>
									<?= $_product->get_image(); ?>
									<div class="header-cart-item-details">
										<?php $price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $values, $item ); ?>
										<b><a href="<?= get_permalink( $product_id ); ?>"><?= apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ); ?></a></b><br>
										<span style="color: #aaa"><b><?= $values['quantity']; ?></b> &times;</span> <?= $price; ?><br> 
										<span style="display: block; font-size: 18px; margin-top: 5px;"><?= apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $values['quantity'] ), $values, $item ); ?></span>
									</div>
								</li>
							<?php endforeach; ?>
							<li class="header-cart-total">
								<div><span style="float: left"><b>Total: </b></span><span style="float: right"><?= $woocommerce->cart->get_cart_total(); ?></span></div>
								<a href="<?= site_url('checkout'); ?>"><button class="button alt">Proceed to Checkout</button></a>
							</li>
						<?php else: ?>
							<li class="header-cart-total">
								<?php _e('Your cart is empty.', 'freestore'); ?>
							</li>
						<?php endif; ?>
					</ul>
					
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<span class="header-menu-button"><i class="fa fa-bars"></i><span><?php echo esc_attr( get_theme_mod( 'freestore-header-menu-text', 'menu' ) ); ?></span></span>
			<div id="main-menu" class="main-menu-container">
				<div class="main-menu-close"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></div>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</div>
		</nav><!-- #site-navigation -->
		
		<div class="clearboth"></div>
	</div>
		
</header><!-- #masthead -->